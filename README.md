# OpenML dataset: EEGEyeState

https://www.openml.org/d/41193

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Description**: All data is from one continuous EEG measurement with the Emotiv EEG Neuroheadset. The duration of the measurement was 117 seconds. The eye state was detected via a camera during the EEG measurement and added later manually to the file after analysing the video frames. '1' indicates the eye-closed and '0' the eye-open state. All values are in chronological order with the first measured value at the top of the data.
**Author**: Oliver Roesler  
Oliver Roesler  
**Source**: [original](https://archive.ics.uci.edu/ml/datasets/EEG+Eye+State) - 2013-06-10  
**Please cite**:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41193) of an [OpenML dataset](https://www.openml.org/d/41193). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41193/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41193/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41193/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

